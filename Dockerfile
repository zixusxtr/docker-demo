FROM microsoft/iis

RUN powershell add-windowsfeature web-asp-net45 

RUN md c:\WebApp
WORKDIR c:/WebApp
COPY . c:/WebApp

EXPOSE 8081
RUN powershell New-Website -Name 'JenkinsTry' -Port 8081 -PhysicalPath 'c:\WebApp\DockerDemo'

# Add another website to the same container by assigning different port:
# EXPOSE <other-port>
# RUN powershell New-Website -Name '<name-of-the-other-website>' -Port <other-port> -PhysicalPath 'c:\WebApp\<path\to\other\website\root>'